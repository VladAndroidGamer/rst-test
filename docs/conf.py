## conf.py

html_title = 'Test'
master_doc = 'index'
project = u'Test Documentation'
version = 'latest'
release = 'latest'
htmlhelp_basename = 'Test Documentation'

# These folders are copied to the documentation's HTML output
html_static_path = ['_static']

# These paths are either relative to html_static_path
# or fully qualified paths (eg. https://...)
html_css_files = [
    'css/hacks.css',
]